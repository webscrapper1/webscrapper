package webscrapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.sql.*;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.opencsv.CSVWriter;

public class Webscrapper {
	public static void main(String[] args) throws Exception {
		
	
	    String url = "https://www.theverge.com";
	    Document doc = Jsoup.connect(url).get();
	    Elements articles = doc.select("div.c-entry-box--compact__body");
	    
	    Connection conn = DriverManager.getConnection("jdbc:mysql:verge.db");
	    Statement stmt = conn.createStatement();
	    stmt.execute("CREATE TABLE IF NOT EXISTS articles (id INTEGER PRIMARY KEY, url TEXT, headline TEXT, author TEXT, date TEXT)");

	    for (Element article : articles) {
	      String headline = article.select("h2.c-entry-box--compact__title").text();
	      String link = article.select("a.c-entry-box--compact__image-wrapper").attr("href");
	      String author = article.select("div.c-byline__author-name").text();
	      String date = article.select("div.c-byline__item").last().text();

	      PreparedStatement ps = conn.prepareStatement("INSERT OR IGNORE INTO articles (url, headline, author, date) VALUES (?, ?, ?, ?)");
	      ps.setString(1, link);
	      ps.setString(2, headline);
	      ps.setString(3, author);
	      ps.setString(4, date);
	      ps.executeUpdate();
	    }

	    conn.close();
	    
	    //Storind DATA into a CSV file
	    String fileName = "ddmmyyy_verge.csv";

	    // Retrieve the data from the SQLite database and store it in a ResultSet object
	    ResultSet resultSet = retrieveDataFromDatabase();

	    // Create the CSVWriter object with the file name
	    try (CSVWriter writer = new CSVWriter(new FileWriter(fileName))) {
	      // Create the header row
	      String[] header = {"id", "URL", "headline", "author", "date"};
	      writer.writeNext(header);

	      // Create a list of article data
	      List<String[]> data = new ArrayList<>();
	      while (resultSet.next()) {
	        String[] article = {
	          resultSet.getString("id"),
	          resultSet.getString("url"),
	          resultSet.getString("headline"),
	          resultSet.getString("author"),
	          resultSet.getString("date")
	        };
	        data.add(article);
	      }

	      // Write the article data to the CSV file
	      writer.writeAll(data);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    
	}    
	    


	private static ResultSet retrieveDataFromDatabase() {
		String url = "jdbc:mysql:verge.db";
		ResultSet resultSet = null;
		try (Connection conn = DriverManager.getConnection(url)) {
		      System.out.println("Connection to SQLite has been established.");

		      // Create a statement object
		      Statement statement = conn.createStatement();

		      // Execute a query and retrieve the result set
		      resultSet = statement.executeQuery("SELECT id, url, headline, author, date FROM articles");
		      }

		     catch (Exception e) {
		      e.printStackTrace();
		    }
		return resultSet;

	    
	}
}
	

